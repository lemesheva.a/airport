import datetime
class Airport_worker():
    def __init__(self, surname, name, surname_two, years_brith, years_works, experience, job_tiyle, gender, adress, city, phone):
        self.surname = surname
        self.name = name
        self.surname_two = surname_two
        self.years_brith = years_brith
        self.years_works = years_works
        self.experience = experience
        self.job_tiyle = job_tiyle
        self.gender = gender
        self.adress = adress
        self.city = city
        self.phone = phone

    def up_experience(self):
        time_today = (input('Сегоднишняя дата (дд.мм.гггг): '))
        time_today = time_today.split('.')       
        years_works = (self.years_works).split('.')
        time_today = datetime.date(int(time_today[2]),int(time_today[1]),int(time_today[0]))
        years_works = datetime.date(int(years_works[2]),int(years_works[1]),int(years_works[0]))
        difference = (str(time_today-years_works)).split(' ')
        difference_day = int (difference[0])
        if difference_day // 365 > 0:
            return self.experience + (difference_day // 365)
        else:
            return self.experience
        
    def change_adress(self):
        self.adress = input('Сменить адрес:')
        return self.adress
    
    def change_city(self):
        self.city = input('Сменить город:')
        return self.city
    
    def change_phone(self):
        self.phone = input('Сменить номер телефона:')    
        return self.phone   




class Flight_schedule():
    def __init__(self, aircraft, date_of_departure, time_of_departure, place_of_departure, place_of_arrival, route, ticket_price):
        self.aircraft = aircraft
        self.date_of_departure = date_of_departure
        self.time_of_departure = time_of_departure
        self.place_of_departure = place_of_departure
        self.place_of_arrival = place_of_arrival
        self.route = route
        self.ticket_price = ticket_price

    def change_aircraft(self):
        self.aircraft = input('Сменить самолет:') 
        return self.aircraft

    def change_ticket_price(self):
        self.ticket_price = input('Сменить цену билета:') 
        return self.ticket_price



class Aircraft():
    def __init__(self, number, year_of_manufacture, number_of_seats, load_capacity):
        self.number = number
        self.year_of_manufacture = year_of_manufacture
        self.number_of_seats = number_of_seats
        self.load_capacity = load_capacity

    def change_load_capacity(self):
        self.load_capacity = int(input('Сменить грузоподъемность:') )
        return self.load_capacity



class Aircraft_brigades():
    def __init__(self, brigade_number, aircraft, airport_worker):
        self.brigade_number = brigade_number
        self.aircraft = aircraft
        self.airport_worker = airport_worker

    def change_brigade_number(self):
        self.brigade_number = int(input('Сменить бригаду:'))
        return self.brigade_number




class ticket_sales_sheet():
    def __init__(self, date_of_sale, time_of_sale, full_name_of_the_passenger, passport_details, flight_number, number_of_tickets, privileges, baggage, cost):
        self.date_of_sale = date_of_sale
        self.time_of_sale = time_of_sale
        self.full_name_of_the_passenger = full_name_of_the_passenger
        self.passport_details = passport_details
        self.flight_number = flight_number
        self.number_of_tickets = number_of_tickets
        self.privileges = privileges
        self.baggage = baggage
        self.cost = cost

    def change_privileges(self):
        self.privileges = input(('Сменить льготу:'))
        return self.privileges



if __name__ == '__main__':
    #fc = Flight_schedule('Letable', '15.12.2023', '18:40', 'Arhangelsk', 'Ufa', 'Arhangelsk - st.Petersburg - Ufa', '4598'
    aw = Airport_worker('Иванов','Иван','Иванович','11.08.96','01.10.2020',2,'Диспетчер','М','Кирова 42 кв.20','Москва', '89514621226')
    print(aw.up_experience)